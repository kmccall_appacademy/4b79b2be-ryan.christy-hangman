class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess
    arr_of_indices = @referee.check_guess(guess)
    update_board(arr_of_indices, guess) if !arr_of_indices.empty?
    @guesser.handle_response(guess, arr_of_indices)
  end

  def update_board(arr, guess)
    arr.each{ |idx| @board[idx] = guess }
    @guesser.current_board = @board
    @referee.current_board = @board
  end

end


class HumanPlayer
  attr_reader :dictionary, :secret_word, :secret_length, :guess, :current_board

  def initialize(dict)
    @dictionary = dict
    @secret_word = ""
    @secret_length = 0
    @current_board = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    # @secret_length
  end

  def guess
    @guess = gets.chomp
    @guess
  end

  def check_guess(x)
    arr = @secret_word.chars.dup
    arr.each_index.select{|i| arr[i] == x  }
  end

  def handle_response(arg1, arr_of_indices)
    guess until @guess != arg1 && arr_of_indices.include?(@guess)
  end

end


class ComputerPlayer
  attr_reader :dictionary, :secret_word, :secret_length, :guess, :candidate_words, :current_board

  def initialize(dict)
    @dictionary = dict
    @secret_word = ""
    @current_board = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @candidate_words = @dictionary.select{|word| word.length == @secret_length}
  end

  def guess(board=[])
      arr1 = @candidate_words.join('').chars
      arr2 = @candidate_words.join('').chars.uniq
      cand = arr2 & board
      cand.each {|el| arr2.delete(el) }
      @guess = arr2.reduce do |acc, letter|
        arr1.count(letter) > arr1.count(acc) ? letter : acc
      end

      @guess
  end

  def check_guess(x)
    arr = @secret_word.chars.dup
    arr.each_index.select{|i| arr[i] == x  }
  end


=begin
  def take_turn
    guess = @guesser.guess
    arr_of_indices = @referee.check_guess(guess)
    update_board(arr_of_indices, guess) if !arr_of_indices.empty?
    @guesser.handle_response(guess, arr_of_indices)
  end

  def check_guess(x)
    arr = @secret_word.chars.dup
    arr.each_index.select{|i| arr[i] == x  }
  end

  def handle_response(arg1, arr_of_indices)
    guess until @guess != arg1 && arr_of_indices.include?(@guess)
  end

=end

  def handle_response(letter, arr_of_indices)

    if !arr_of_indices.empty?

      @candidate_words.each do |word|
        arr = word.chars.each_index.select{|i| word[i] == letter}
        if arr != arr_of_indices
          @candidate_words.delete(word)
          next
        end
      end

    else

      @candidate_words.each do |word|
        if word.include?(letter)
          @candidate_words.delete(word)
          next
        end
      end

    end

  end

  def update_candidate_words
    @candidate_words.each do |word|
      word.chars.each.with_index do |char, idx|
        #delete the word if the count of this letter in the word and the count of this letter in the board is different.
        if @current_board[idx] != nil && @current_board[idx] != char
          @candidate_words.delete(word)
          break
        end
      end
    end
  end

end
